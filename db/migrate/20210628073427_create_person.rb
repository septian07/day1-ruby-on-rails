class CreatePerson < ActiveRecord::Migration[6.1]
  def change
    create_table :people do |t|

      t.string :name
      t.string :title
      t.text :content
      t.timestamps
    end
  end
end
